package com.deepDive.lists.linkedList;

import com.deepDive.lists.Employee;

import java.util.Iterator;
import java.util.LinkedList;

public class LinkedListExample {

    public static void main(String[] args) {
        Employee employee = new Employee("Jane", "Jones", 123);
        Employee employee1 = new Employee("John", "Doe", 4567);
        Employee employee2 = new Employee("Mary", "Smith", 22);
        Employee employee3 = new Employee("Mike", "Willson", 3245);
        Employee employee4 = new Employee("Bill", "End", 78);

        LinkedList<Employee> list = new LinkedList<>();
        list.add(employee1);
        list.add(employee2);
        list.add(employee3);
        list.add(employee);
        Iterator iter = list.iterator();

        System.out.println("HEAD -> ");
        while (iter.hasNext()) {
            System.out.println(iter.next());
            System.out.println("<=>");
        }
        System.out.println("null");

        //adds employee at the end of the list
//        list.add(employee4);
        list.addFirst(employee4);
        iter = list.iterator();
        System.out.println("HEAD -> ");
        while (iter.hasNext()) {
            System.out.println(iter.next());
            System.out.println("<=>");
        }
        System.out.println("null");




    }
}
