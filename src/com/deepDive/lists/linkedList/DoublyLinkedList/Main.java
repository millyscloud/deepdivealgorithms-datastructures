package com.deepDive.lists.linkedList.DoublyLinkedList;

import com.deepDive.lists.Employee;

public class Main {
    public static void main(String[] args) {

        Employee employee = new Employee("Jane", "Jones", 123);
        Employee employee1 = new Employee("John", "Doe", 4567);
        Employee employee2 = new Employee("Mary", "Smith", 22);
        Employee employee3 = new Employee("Mike", "Willson", 3245);

        EmployeeDoublyLInkedList linkedList = new EmployeeDoublyLInkedList();
        linkedList.addToEnd(employee);
        linkedList.addToEnd(employee1);
        linkedList.addToEnd(employee2);
        linkedList.addToEnd(employee3);


//        System.out.println(linkedList.getSize());
//
        Employee billEnd = new Employee("Bill", "End", 78);
        linkedList.addToEnd(billEnd);
        linkedList.printList();
//        linkedList.printList();
//        System.out.println(linkedList.getSize());
//
//        linkedList.removeFromFront();
//        linkedList.printList();
//        System.out.println(linkedList.getSize());
//
//        linkedList.removeFromEnd();
//        linkedList.printList();
//        System.out.println(linkedList.getSize());

        linkedList.addBefore(billEnd, employee1);
        linkedList.addBefore(new Employee("Some", "One", 123), employee3);
        linkedList.printList();
    }

}
