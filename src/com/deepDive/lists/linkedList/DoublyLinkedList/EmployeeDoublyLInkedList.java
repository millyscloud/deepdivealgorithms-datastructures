package com.deepDive.lists.linkedList.DoublyLinkedList;

import com.deepDive.lists.Employee;

public class EmployeeDoublyLInkedList {

    private EmployeeNode head;
    private EmployeeNode tail;
    private int size;

    public void addToFront(Employee employee) {
        EmployeeNode node = new EmployeeNode(employee);
        node.setNext(head);
        head = node;
        size++;
    }

    public void addToEnd(Employee  employee) {
        EmployeeNode node = new EmployeeNode(employee);
        if(tail == null) {
            head = node;
        }
        else {
            tail.setNext(node);
            node.setPrevious(tail);
        }
        tail = node;
        size++;

    }
    public boolean addBefore(Employee employeeAdd, Employee employeeExisting) {
        //check if list is empty
        if(isEmpty()) {
            return false;
        }
        EmployeeNode current = head;
        //check if exsting employee really exists in the list
        while (current != null && current.getEmployee().equals(employeeExisting)) {
            current = current.getNext();

        }
        //
        if (current == null) {
            return false;
        }

        EmployeeNode newNode = new EmployeeNode(employeeAdd);
        newNode.setPrevious(current.getPrevious());
        newNode.setNext(current);
        current.setPrevious(newNode);

        if(head == current) {
            head = newNode;
        } else {
            newNode.getPrevious().setNext(newNode);
        }
        size++;
        return true;
    }

    public int getSize() {
        return size;
    }

    public void printList() {
        EmployeeNode current = head;
        System.out.println("HEAD -> ");
        while(current != null) {
            System.out.println(current);
            System.out.println(" ->");
            current = current.getNext();
        }
        System.out.println("null");
    }

    public boolean isEmpty() {
        return head == null;
    }

    public EmployeeNode removeFromFront() {
        if (isEmpty()) {
            return null;
        }

        EmployeeNode removedNode = head;

        if (head.getNext() == null) {
            tail = null;
        } else {
            head.getNext().setPrevious(null);
        }
        head = head.getNext();
        size--;
        removedNode.setNext(null);
        return removedNode;
    }

    public EmployeeNode removeFromEnd() {
        if(isEmpty()) {
            return null;
        }

        EmployeeNode removedNode = tail;
        if (tail.getPrevious() == null) {
            head = null;
        } else {
            tail.getPrevious().setNext(null);
        }
        tail = tail.getPrevious();
        size--;
        removedNode.setPrevious(null);
        return removedNode;
    }




}
