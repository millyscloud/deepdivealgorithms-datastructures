package com.deepDive.lists.linkedList.SinglyLinkedList;

import com.deepDive.lists.Employee;

public class Main {

    public static void main(String[] args) {

        Employee employee = new Employee("Jane", "Jones", 123);
        Employee employee1 = new Employee("John", "Doe", 4567);
        Employee employee2 = new Employee("Mary", "Smith", 22);
        Employee employee3 = new Employee("Mike", "Willson", 3245);

        EmployeeSinglyLinkedList linkedList = new EmployeeSinglyLinkedList();
        linkedList.addToFront(employee);
        linkedList.addToFront(employee1);
        linkedList.addToFront(employee2);
        linkedList.addToFront(employee3);

        linkedList.printList();

        System.out.println(linkedList.getSize());
        linkedList.removeFromFront();
        System.out.println(linkedList.getSize());
        linkedList.printList();
    }


}
