package com.deepDive.lists.list;

import com.deepDive.lists.Employee;

import java.util.ArrayList;
import java.util.List;

public class ListExample {

    public static void main(String[] args) {
        //Array List's default capacity is 10 elements
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(new Employee("Jane", "Jones", 123));
        employeeList.add(new Employee("John", "Doe", 4567));
        employeeList.add(new Employee("Mary", "Smith", 22));
        employeeList.add(new Employee("Mike", "Willson", 3245));

//        employeeList.forEach(System.out::println);
//        System.out.println(employeeList.get(1));
//        System.out.println(employeeList.isEmpty());
//        employeeList.set(1,new Employee("Jane", "Doe", 4568));
//        employeeList.forEach(System.out::println);
//        System.out.println(employeeList.size());
//
//      employeeList.add(3,new Employee("John", "Doe", 4567));
//        employeeList.forEach(System.out::println);
//        System.out.println(employeeList.size());
        //list to array
        Employee[] employeeArray = employeeList.toArray(new Employee[employeeList.size()]);
        for(Employee employee : employeeArray) {
            System.out.println(employee);
        }

//        System.out.println(employeeList.contains(new Employee("John", "Doe", 4567)));
    }
}
