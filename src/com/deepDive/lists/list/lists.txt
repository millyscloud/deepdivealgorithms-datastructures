List is an abstract data type, it's not a data structure.

Interface List<E>
-user can access elements by their integer index
-lists allow duplicate elements
-methods: add, contains, isEmpty, remove, find, size,toArray ...

Abstract Data type
-doesnt dictate how the data is organized
-dictates the operations you can perform
#concrete data structure is usually a concrete class
#abstract data type is usually an interface

Array List's default capacity is 10 elements, if list exceeds 10 elements
it is implicitly copied to a larger array.
Arrays are great for searching by index because they search at a constant time(just like in an array)


