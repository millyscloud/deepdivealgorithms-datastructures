Tree is a hierarchical data structure.
-Each node can have only one parent.
-Special node is Root Node - it has no parent.

Examples:
Class can have only one parent.
Folder/File can have oney one parent.

Every item in the tree is a NODE.
The node at the top of the tree is the ROOT.
Every non-root node has one and only one parent.
A LEAF node has no children.
A singleton tree has only one node - the root.

Every tree consists of subtrees.
Path - sequence to go through from one node to another node.

## Binary Tree ##
Every node has 0,1 or 2 children
Children are referred to as LEFT child and RIGHT child.
In practice we use binary search tree.
Full binary trees are complete binary trees(every node has 2 children until the bottom level)
