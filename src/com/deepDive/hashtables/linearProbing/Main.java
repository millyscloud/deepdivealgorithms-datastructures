package com.deepDive.hashtables.linearProbing;

import com.deepDive.hashtables.Employee;
import com.deepDive.hashtables.linearProbing.LinearProbingHashtable;

public class Main {

    public static void main(String[] args) {
        com.deepDive.hashtables.Employee employee = new com.deepDive.hashtables.Employee("Jane", "Jones", 123);
        com.deepDive.hashtables.Employee employee1 = new com.deepDive.hashtables.Employee("John", "Doe", 4567);
        com.deepDive.hashtables.Employee employee2 = new com.deepDive.hashtables.Employee("Mary", "Smith", 22);
        com.deepDive.hashtables.Employee employee3 = new Employee("Mike", "Willson", 3245);

        LinearProbingHashtable ht = new LinearProbingHashtable();
        ht.put("Emp", employee);
        ht.put("Empl1", employee1);
        ht.put("Emplo2", employee2);
        ht.put("Employee3", employee3);

        ht.printHashtable();

        System.out.println("Retrieve key: " + ht.get("Emplo2").toString());
        System.out.println("Retrieve key: " + ht.get("Employee3").toString());

        ht.remove("Emp");

        ht.printHashtable();

    }
}
