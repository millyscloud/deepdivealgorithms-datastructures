Bucket sort
uses hashing -the values that are sorted are hashed
-it makes assumptions about the data and it can sort in O(n) time
-Performs best when hashed values of item being sorted are evenly distributed, so there aren't many collisions

Scattering - distributing items into buckets based on their hashed values
-then we srt items in each bucket
-then we merge the buckets, concatenating buckets - gathering phase

THe values in bucket X must be greater than the values in bucket X - 1 and less than the values in bucket X + 1

Not in-place algorithm
stability depends on the algorithm used to sort buckets
O(n)- cak be achieved if there is only one item per bucket


