package com.deepDive.hashtables;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        com.deepDive.hashtables.Employee employee = new com.deepDive.hashtables.Employee("Jane", "Jones", 123);
        com.deepDive.hashtables.Employee employee1 = new com.deepDive.hashtables.Employee("John", "Doe", 4567);
        com.deepDive.hashtables.Employee employee2 = new com.deepDive.hashtables.Employee("Mary", "Smith", 22);
        com.deepDive.hashtables.Employee employee3 = new Employee("Mike", "Willson", 3245);

        Map<String, Employee> hashMap = new HashMap<>();
        hashMap.put("emp", employee);
        hashMap.put("emp1", employee1);
        hashMap.put("emp2", employee2);
        hashMap.put("emp3", employee3);

        Iterator<Employee> iterator = hashMap.values().iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        hashMap.forEach((k,v) -> System.out.println(k + " : " +  v));

        hashMap.putIfAbsent("emp", employee2);

        System.out.println(hashMap.getOrDefault("emp", employee));
    }
}
