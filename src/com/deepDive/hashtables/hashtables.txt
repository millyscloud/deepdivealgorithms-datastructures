-abstract data type, can be backed differently
-provides access to data using keys, you need to key to access the value
-consists of key value pairs
-it can be backed by anything
-optimized for retrieval(if you know the key)

-array index is the key and the value is the element in the array(array index doesnt have to be integer)
-associative arrays are one type of hash table

- They are called dictionaries,maps and associative arrays

Anything can serve as a key but those keys are converted to integers
Common way is to back hash table by arrays(keys are hashed)
Hashing maps keys of any data type to an integer
-hash function maps keys to int
-In Java Hash Function is Object.hashCode()

It's possible that the hashing method may produce the same integer for more than one value- Collision
-Collision occurs when more than one value has the same hashed value

Keys can be of any type(they are hashed).

Hashing
-keys of any data type are mapped to an integer
-Hash function Object.hasCode() maps keys to integer
-It's possible that the hashing method produces the same value
for the same object, it's called a collision.

#Standard hashCode() implementation
//@Override
//public int hashCode() {
//    int hash = 7;
//    hash = 31 * hash + (int) id;
//    hash = 31 * hash + (name == null ? 0 : name.hashCode());
//    hash = 31 * hash + (email == null ? 0 : email.hashCode());
//    return hash;
//}

Load Factor
-tells us how full a hash table is
Load factor = number of items / capacity = size/capacity e.g. 5/10 = 50% capacity - Load factor is 0.5
low Load Factor - means a lot of unused space,
high Load Factor - means that collisions are more likely.
-> it is used to decide when to resize the array backing the hash table

How to Add to a Hash Table backed by an array
1.Provide a key/value pair
2.Use the hash function to has the key to an int value
3.Retrieve the value stored at the hashed key value

How to Retrieve a value from a hashtable
1.Provide the key
2.Use the same has function to hash the key to an int value
3.Retrieve the value stored at the hashed key value


How to handle collisions -  the hashing method produces the same value
                           for the same object
2 strategies
1. Open Addressing - if we try to put an object into the table and there is already an employee
in the slot, then we look for another position in the array
we use - Linear probing - when we discover that the position is already occupied we
increment the position by 1
