package com.deepDive.hashtables.challenge2;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        LinkedList<Employee> employees = new LinkedList<>();
        employees.add(new Employee("Jane", "Jones", 123));
        employees.add(new Employee("John", "Doe", 5678));
        employees.add(new Employee("Mike", "Wilson", 45));
        employees.add(new Employee("Mary", "Smith", 5555));
        employees.add(new Employee("John", "Doe", 5678));
        employees.add(new Employee("Bill", "End", 3948));
        employees.add(new Employee("Jane", "Jones", 123));

        employees.forEach(e -> System.out.println(e));

        Map<Integer, Employee> employeeMap = new HashMap<>();
        ListIterator<Employee> listIterator = employees.listIterator();
        List<Employee> remove = new ArrayList<>();

        while(listIterator.hasNext()) {
            Employee current = listIterator.next();
            if(employeeMap.containsKey(current.getId())) {
                remove.add(current);
            } else {
                employeeMap.put(current.getId(), current);
            }
        }
        for(Employee employee : remove) {
            employees.remove(employee);
        }

        employees.forEach(e -> System.out.println("New" + e));





    }

    public static int hash(int value) {
        return Math.abs(value % 10);
    }
}
