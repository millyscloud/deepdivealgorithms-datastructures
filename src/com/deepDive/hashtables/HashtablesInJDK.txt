Primary interface for hashtables is is Map<k,V>

-map cannot contain duplicate keys

HashMap class implements Map interface
-permits null values and null keys
-the implementation is not synchronised
Collections.synchronizedMap(); - method for synchronization

LinkedHashMap<K,V>
-a hash table is backed by an array
-it uses doubly linked list
it's not synchronized