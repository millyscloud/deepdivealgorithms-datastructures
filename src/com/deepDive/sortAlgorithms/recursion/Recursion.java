package com.deepDive.sortAlgorithms.recursion;

public class Recursion {

    public static void main(String[] args) {
        int[] intArray = { 55, 35, -15, 7, 55, 1, -22 };


        for(int i =0; i < intArray.length; i++){
            System.out.println(intArray[i]);
        }
    }
//tail reursion
    public static int recursiveFactorial (int num) {
        if (num == 0){
            return 1;
        }
        return num * recursiveFactorial(num - 1);

    }
    //1! = 1 * 0! = 1
    //2! = 2 * 1 - 2 * 1!
    public static int iterativeFactorial(int num){
        if(num == 0){
            return 1;
        }
        int factorial = 1;
        for(int i = 1; i <= num; i++){
            factorial *= i;
        }
        return factorial;
    }
}
