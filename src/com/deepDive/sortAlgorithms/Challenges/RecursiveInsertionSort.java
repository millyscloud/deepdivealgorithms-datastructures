package com.deepDive.sortAlgorithms.Challenges;

public class RecursiveInsertionSort {

    public static void main(String[] args) {

        int[] intArray = { 55, 35, -15, 7, 55, 1, -22 };
        recursiveInsertionSort(intArray, 1);


        for(int i =0; i < intArray.length; i++){
            System.out.println(intArray[i]);
        }

    }

    public static void recursiveInsertionSort(int[] intArray, int firstUI) {
        if(firstUI == intArray.length) {
            return;
        }
        int newElement = intArray[firstUI];
        int i;
        for(i = firstUI; i > 0 && intArray[i-1] > newElement; i--){
            intArray[i] = intArray[i - 1];
        }
        intArray[i] = newElement;

        recursiveInsertionSort(intArray, ++firstUI);

    }
}
