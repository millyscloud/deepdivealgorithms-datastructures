package com.deepDive.sortAlgorithms.insertionSort;

public class ShellSort {

    public static void main(String[] args) {
        int[] intArray = { 55, 35, -15, 7, 55, 1, -22 };
        //initialize gap value and reduce it on each iteration
        //the final iteration has a gap of 1, it is actual insertion sort algorithm
        for(int gap = intArray.length / 2; gap > 0; gap /= 2) {
            //i is gap, which should be 1
            //
            for(int i = gap; i<intArray.length; i++){
                int newElement = intArray[i];
                int j = i;
                while( j >= gap && intArray[j - gap] > newElement) {
                    intArray[j] = intArray[j - gap];
                    j -= gap;
                }
                intArray[j] = newElement;
            }
        }

        for(int i =0; i < intArray.length; i++){
            System.out.println(intArray[i]);
        }
    }
}
