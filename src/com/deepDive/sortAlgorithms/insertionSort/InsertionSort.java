package com.deepDive.sortAlgorithms.insertionSort;

public class InsertionSort {

    public static void main(String[] args) {

        int[] intArray = { 55, 35, -15, 7, 55, 1, -22 };
//         first element is sorted, the rest are considered to be unsorted
//        we start from the second element with position 1
//        we finish at firstUI < intArray.length
        for (int firstUI = 1; firstUI <intArray.length; firstUI++) {
            //we save the first unsorted element
            int newElement = intArray[firstUI];
            //we decalre i before the for loop
            int i;
            //we traverse the sorted section
            //we start from i = first unsorted element
            //we search until we hit the first element AND
            //until the previous element is larger than our unsorted element
            //we decrement ii because we search backwards-from right to left
            for(i = firstUI; i > 0 && intArray[i-1] > newElement; i--){
                //we shift the element from left to right
                intArray[i] = intArray[i - 1];
            }
                //we move the new element to the left, to its appropriate place
            intArray[i] = newElement;

        }

        for(int i =0; i < intArray.length; i++){
            System.out.println(intArray[i]);
        }
    }
}
