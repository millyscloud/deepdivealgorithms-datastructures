package com.deepDive.sortAlgorithms.bubbleSort;

public class ShakerBubbleSort {

    public static void main(String[] args) {

        int[] array = { 55, 35, -15, 7, 55, 1, -22 };

        for (int i = 0; i < array.length/2; i++) {
            boolean swapped = false;
            for (int j = i; j < array.length - i - 1; j++) {
                if (array[j] > array[j+1]) {
                    int tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                    swapped = true;
                }
            }
            for (int j = array.length - 2 - i; j > i; j--) {
                if (array[j] < array[j-1]) {
                    int tmp = array[j];
                    array[j] = array[j-1];
                    array[j-1] = tmp;
                    swapped = true;
                }
            }
            if(!swapped) break;
        }
        for(int i =0; i < array.length; i++){
            System.out.println(array[i]);

        }
}
}
