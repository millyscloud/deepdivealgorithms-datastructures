package com.deepDive;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        int[] intArray = { 55, 35, -15, 7, 55, 1, -22 };
        List<String> list = new ArrayList<>();
        list.add("fsfa");

        //Arrays.sort uses QuickSort
        //Arrays.parallelSort uses MergeSort for larger data sets
        //and QuickSOrt for smaller data sets

        Arrays.parallelSort(intArray);
        for(int i =0; i < intArray.length; i++){
            System.out.println(intArray[i]);
        }
    }
}
