package com.deepDive.stacks;

import java.util.EmptyStackException;

public class ArrayStack {

    private Employee[] stack;
    //next available position at the top, if there is one element next is 1
    private int top;

    public ArrayStack(int capacity) {
        stack = new Employee[capacity];
    }

    public void push(Employee employee) {
        if(top == stack.length) {
            //need to resize the backing array
            Employee[] newArray = new Employee[2 * stack.length];
            //copy array stack from element at position 0,
            //to new Array from element at position 0 to position stack.length
            System.arraycopy(stack, 0, newArray, 0, stack.length);
            stack = newArray;

        }
        stack[top++] = employee;
    }

    public Employee pop() {
        if(isEmpty()) {
            throw new EmptyStackException();
        }
        Employee employee = stack[--top];
        stack[top] = null;
        return employee;
    }

    public Employee peek() {

        if(isEmpty()){
            throw new EmptyStackException();
        }
        return stack[top - 1];
    }

    public int size() {
        return top;
    }

    public boolean isEmpty() {
        return top == 0;
    }

    public void printStack() {
        System.out.println("Top");
        System.out.println();
        for(int i = top-1;i >= 0; i--) {
            System.out.println(stack[i]);
        }
        System.out.println();
        System.out.println("Bottom");
    }


}
