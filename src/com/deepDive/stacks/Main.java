package com.deepDive.stacks;

import java.util.Stack;

public class Main {

//stack is an abstract data type - can be backed by different data structures
// LIFO - last in forst out
// operations at constant time O(1)
// push - adds an item as the tom item on the stack
// pop - removes the top item on the stack
// peek - gets the top item on the stack without popping it
// Ideal backing data structure - Singly Linked List - always points at the front of the list(top of the stack)
//you always work with the first item - item at the top

    public static void main(java.lang.String[] args) {
        System.out.println("ArrayStack");
        ArrayStack stack = new ArrayStack(1);
        Employee employee = new Employee("Jane", "Jones", 123);
        Employee employee1 = new Employee("John", "Doe", 4567);

        stack.push(employee);
        stack.push(employee1);
        stack.printStack();

        System.out.println("Peeked:" + stack.peek());
        stack.pop();
        stack.printStack();
        System.out.println("Is this string a palindrome?" + checkForPalindrome("King, are you glad you are king?"));
        System.out.println("Is this string a palindrome?" + checkForPalindrome("redivid(er."));
        System.out.println("Is this string a palindrome?" + checkForPalindrome("Was it a car or a cat I saw?"));

//        System.out.println("LinkedStack");
//        LinkedStack linkedStack = new LinkedStack();
//        linkedStack.push(string);
//        linkedStack.push(string1);
//        linkedStack.printStack();
    }

    public static boolean checkForPalindrome(java.lang.String string) {
        Stack stack = new Stack();
        string = string.replaceAll("[^A-Za-z]+", "").toLowerCase();
//        if(string.length() % 2 != 0) {
//            System.out.println("Odd number of letters, therefore string cannot be a palindrome!");
//            return false;
//        }

        for(int i  = 0; i < string.length(); i++) {
            stack.push(string.substring(i, i+1));
        }

        for(int i = 0 ; i < stack.size()/2; i++) {
            if(stack.peek().equals(stack.elementAt(i).toString())) {
                stack.pop();
            } else {
                return false;
            }
        }
        return true;
    }
}
