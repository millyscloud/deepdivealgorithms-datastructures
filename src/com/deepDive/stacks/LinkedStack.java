package com.deepDive.stacks;

import java.util.LinkedList;
import java.util.ListIterator;

public class LinkedStack {
    //jdk class Stack extends Vector class
    //Deque interface is recommended to use instead of class Stack<E>
    //Deque<Integer> stack = new ArrayDeque<Integer>
    //Linked list is also a great choice

    private LinkedList<String> stack;

    public LinkedStack() {

        stack = new LinkedList<String>();
    }

    public void push(String employee) {
        stack.push(employee);
    }

    public String pop() {
        return stack.pop();
    }

    public String peek() {
        return stack.peek();
    }

    public boolean isEmpty() {
        return stack.isEmpty();
    }

    public void printStack() {
        ListIterator<String> iterator = stack.listIterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    public int getSize() {
        return stack.size();
    }


}
