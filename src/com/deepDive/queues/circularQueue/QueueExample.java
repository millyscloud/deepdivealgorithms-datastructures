package com.deepDive.queues.circularQueue;

import com.deepDive.queues.ArrayQueue;
import com.deepDive.queues.Employee;

public class QueueExample {

// abstract data types
// FIFO- first in, first out
// add - also called ENQUEQUE - add an item to the end of the queue
// remove - also called DEQUEUE - remove the item at the front of the queue
// peek - get the item at the front of the queue, but don't remove it
// two popular implementations - arrays and linked lists
    public static void main(String[] args) {
        Employee employee = new Employee("Jane", "Jones", 123);
        Employee employee1 = new Employee("John", "Doe", 4567);
        Employee employee2 = new Employee("Mary", "Smith", 22);
        Employee employee3 = new Employee("Mike", "Willson", 3245);
        Employee employee4 = new Employee("Bill", "End", 78);

        ArrayQueue queue = new ArrayQueue(5);
        queue.add(employee);
        queue.add(employee1);
        queue.remove();
        queue.add(employee);
        queue.add(employee1);
        queue.remove();
        queue.add(employee);
        queue.add(employee1);
        queue.remove();
        queue.add(employee);
        queue.add(employee1);
        queue.remove();

        queue.printQueue();

    }
}
