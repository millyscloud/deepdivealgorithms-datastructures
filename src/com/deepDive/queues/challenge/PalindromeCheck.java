package com.deepDive.queues.challenge;

import com.deepDive.queues.ArrayQueue;
import com.deepDive.queues.MyQueue;

import javax.swing.text.html.BlockView;
import java.util.LinkedList;
import java.util.Queue;

public class PalindromeCheck {

    public static void main(String[] args) {
        // should return true
        System.out.println(checkForPalindrome("abccba"));
        // should return true
        System.out.println(checkForPalindrome("Was it a car or a cat I saw?"));
        // should return true
        System.out.println(checkForPalindrome("I did, did I?"));
        // should return false
        System.out.println(checkForPalindrome("hello"));
        // should return true
        System.out.println(checkForPalindrome("Don't nod"));
    }

    public static boolean checkForPalindrome(String string) {
        string = string.replaceAll("[^A-Za-z]+", "").toLowerCase();

        //the same implementation of queue
        MyQueue queue = new MyQueue(string.length());
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = string.length(); i > 0;i--) {
            stringBuilder.append(string, i-1, i);
        }

        return string.equals(stringBuilder.toString());
    }

    public static boolean checkForPalindromeUdemy(String string) {

        LinkedList<Character> stack = new LinkedList<>();
        LinkedList<Character> queue = new LinkedList<>();

        String lowerCase = string.toLowerCase();
        for(int i =0; i < lowerCase.length(); i++) {
            char c = lowerCase.charAt(i);
            if(c >= 'a' && c <= 'z') {
                queue.addLast(c);
                stack.push(c);
            }
        }

        while(!stack.isEmpty()) {
            if(!stack.pop().equals(queue.removeFirst())) {
                return false;
            }
        }
        return true;
    }
}
