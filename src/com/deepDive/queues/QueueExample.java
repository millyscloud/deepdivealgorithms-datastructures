package com.deepDive.queues;

import com.deepDive.stacks.ArrayStack;

public class QueueExample {

// abstract data types
// FIFO- first in, first out
// add - also called ENQUEQUE - add an item to the end of the queue
// remove - also called DEQUEUE - remove the item at the front of the queue
// peek - get the item at the front of the queue, but don't remove it
// two popular implementations - arrays and linked lists
    public static void main(String[] args) {
        com.deepDive.queues.Employee employee = new com.deepDive.queues.Employee("Jane", "Jones", 123);
        com.deepDive.queues.Employee employee1 = new com.deepDive.queues.Employee("John", "Doe", 4567);
        com.deepDive.queues.Employee employee2 = new com.deepDive.queues.Employee("Mary", "Smith", 22);
        com.deepDive.queues.Employee employee3 = new com.deepDive.queues.Employee("Mike", "Willson", 3245);
        com.deepDive.queues.Employee employee4 = new Employee("Bill", "End", 78);

        ArrayQueue queue = new ArrayQueue(5);
        queue.add(employee);
        queue.add(employee1);
        queue.remove();
        queue.printQueue();

    }
}
