package com.deepDive.queues;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.NoSuchElementException;

public class ArrayQueue {

    private Employee[] queue;
    private int front;
    private int back;

    //we add an item to the back, we remove it from the front
    //the back pointer changes when we add an item, back pointer points to the next available position
    //the front pointer changes when we remove an item, points to the next queue item

    public ArrayQueue(int capacity) {
        queue = new Employee[capacity];
    }

    public void add(Employee employee) {
        //since the back points to the next available space,
        //queue is considered to be ful when back = queue.length
        if (back == queue.length) {
        Employee[] newArray = new Employee[2 * queue.length];
        System.arraycopy(queue, 0, newArray, 0, queue.length);
        queue = newArray;
        }
        queue[back] = employee;
        back++;
    }

    public Employee remove() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }

        Employee employee = queue[front];
        queue[front] = null;
        front++;
        if (size() == 0) {
            front = 0;
            back = 0;
        }
        return employee;
    }

    public Employee peek() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }
        return queue[front];
    }

    public int size() {
        return back - front;
    }

    public void printQueue() {
        for(int i = front; i < back; i++) {
            System.out.println(queue[i]);
        }
    }
}
