package com.deepDive.queues;

import java.util.NoSuchElementException;

public class MyQueue {

    private String[] queue;
    private int front;
    private int back;


    public MyQueue(int capacity) {
        queue = new String[capacity];
    }

    public void add(String string) {
        //since the back points to the next available space,
        //queue is considered to be ful when back = queue.length
        if (back == queue.length) {
            String[] newArray = new String[2 * queue.length];
            System.arraycopy(queue, 0, newArray, 0, queue.length);
            queue = newArray;
        }
        queue[back] = string;
        back++;
    }

    public String remove() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }

        String string = queue[front];
        queue[front] = null;
        front++;
        if (size() == 0) {
            front = 0;
            back = 0;
        }
        return string;
    }

    public String peek() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }
        return queue[front];
    }

    public String getLast() {
        return queue[back-1];
    }

    public int size() {
        return back - front;
    }

    public void printQueue() {
        for(int i = front; i < back; i++) {
            System.out.println(queue[i]);
        }
    }
}
